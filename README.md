# release-template

This project contains all the necessary features that are required to get a project going with automatic releases.

## jobs

### verify_commits

Uses [commitlint](https://github.com/conventional-changelog/commitlint) to verify the new commits introduced by a merge request all confirm to the specification of [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/). The template uses the angular preset by default, allowing a wide range of commit types. The job will output the list of checked commits and fails if there was any error during execution.

### prepare_version

This job exports an environment variable called `TAG` which contains the [semantic version](https://semver.org) of the upcoming release. The [git-semver](https://github.com/PSanetra/git-semver) tool checks the latest tags and git logs to deduce which version number part has to be bumped.

### prepare_changelog

The changelog instead of being version controlled in the CHANGELOG.md file is tracked in the git logs. This job will export the changes since the last release, marked by a tag, and export it in a `_CHANGELOG.mg` file. It relies on the same commit format and uses the [conventional-changelog-cli](https://github.com/conventional-changelog/conventional-changelog) to gather the information.

### create_release

This manual job is the entrypoint for the creation of a release. It will use the information provided by `prepare_version` and `prepare_changelog` jobs to create a new Gitlab release in the repository. It will create a tag using the same version, which can be used to trigger additional pipelines for binary distributions.

### build_collector

Meta job that needs to depend on other build tasks. It will ensure that jobs depending on it will only be schedules once all other build jobs are successful. Currently unused by the release-template jobs.

### test_collector

Meta job that needs to depend on other test tasks. It will ensure that jobs depending on it will only be schedules once all other test jobs are successful. Used by create_release job to ensure it can only execute if tests are successful.

### deploy_collector

Meta job that needs to depend on other deploy tasks. It will ensure that jobs depending on it will only be schedules once all other deploy jobs are successful. Currently unused by the release-template jobs.

## project settings

### merge requests

The following settings must be changed to allow the tools to function properly:
- **merge method**: `fast-forward merge`
- **squash commits when merging**: `do not allow`
- **merge checks**: `pipelines must succeed` enabled
